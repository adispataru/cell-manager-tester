import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * Created by adispataru on 09-Jun-16.
 */
public class Application {

    private static final Logger LOG = Logger.getLogger(Application.class.getName());

    public static void main(String[] args) throws InterruptedException {
        CLI cli = new CLI(args);
        cli.parse();
        int maxThreads = cli.getNumThreads();
        int timeLax = cli.getTimeLax();

        ExecutorService executor = Executors.newFixedThreadPool(4);
        Random random = new Random(System.currentTimeMillis());
        for(int i = 0; i < 100; i++){
            LOG.info("Time = " + i);
            int wait = random.nextInt(timeLax);
            LOG.info("Waiting " + wait + "seconds");
            while (wait > 0){
                --wait;
                Thread.sleep(1000);
            }
            int users = random.nextInt(maxThreads);
            LOG.info("Simulating " + users + " concurrent users");
            for(int j = 0; j < users; j++){
                executor.execute(new ResourceTemplatePublisher());
            }
            executor.awaitTermination(200, TimeUnit.MILLISECONDS);
        }
        executor.shutdown();

    }
}
