## Cell Manager Tester ##

This repository contains code for simulating a number of users making requests to the CloudLightning CellManager. 

### How it works? ###

The program expects two arguments when running: one representing the maximum number of users/sec and the other representing the maximum number of seconds when no users may make requests. A single run will consist of 100 steps, and at each step two random numbers are chosen representing the number of users/s and the number of seconds to sleep after the requests (timelax). 

### Set Up ###

#### Requirements: ####
* Maven

#### Usage: ####
1. Change into root directory and execute ``` mvn clean package ```
2. Run the jar packaged in ``` target/queuing-tester-jar-with-dependencies.jar ```

* Example: ``` java -jar ./target/queuing-tester-jar-with-dependencies.jar -n 10 -t 2``` will run 100 steps, in each step making at most `10` requests and providing a time lax of maximum 1 second (random % `2`). 

### Who do I talk to? ###

* Adrian Spataru (adrian.spataru@e-uvt.ro)