import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import eu.cloudlightning.core.datamodel.definition.Blueprint;
import eu.cloudlightning.core.datamodel.definition.HardwareType;
import eu.cloudlightning.core.datamodel.definition.Service;
import eu.cloudlightning.core.datamodel.definition.ServiceImplementation;
import eu.cloudlightning.core.datamodel.definition.dependencies.BinaryDependency;
import eu.cloudlightning.core.datamodel.definition.dependencies.Dependency;
import eu.cloudlightning.core.datamodel.definition.dependencies.RepoDependency;
import eu.cloudlightning.core.datamodel.request.Constraint;
import eu.cloudlightning.core.datamodel.request.ResourceRequest;
import eu.cloudlightning.core.datamodel.request.ResourceTemplate;
import eu.cloudlightning.core.datamodel.request.ServiceRequest;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Logger;

/**
 * Created by adispataru on 10-Jun-16.
 */
public class ResourceTemplatePublisher implements Runnable{


    private static final String queueName = "Resource_request_queue";
    private static final String host = "localhost";
    private static final Logger LOG = Logger.getLogger(ResourceTemplatePublisher.class.getName());
    private static AtomicLong counter = new AtomicLong();

    private void send(String message) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(host);
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.queueDeclare(queueName, true, false, false, null);
        channel.basicPublish("", queueName, null, message.getBytes("UTF-8"));
        System.out.println(" [x] Sent '" + message + "'");
        channel.close();
        connection.close();
    }

    @Override
    public void run() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            send(mapper.writeValueAsString(createResourceTemplate()));
        } catch (IOException | TimeoutException e) {
            LOG.severe(e.getMessage());
        }
    }

    private ResourceTemplate createResourceTemplate() {
        Blueprint bp = getBlueprint();
        ResourceTemplate template = new ResourceTemplate();
        template.setBpId(bp.getId());
        template.setServiceRequestList(new ArrayList<ServiceRequest>());
        for(Service s : bp.getServiceList()){
            ServiceRequest sr = new ServiceRequest();
            sr.setName(s.getName());
            Constraint c = new Constraint();
            c.setAttribute(Constraint.Attribute.COST);
            c.setOperator(Constraint.Operator.LESS_THAN);
            c.setValue(1000.0);
            sr.setConstraints(new ArrayList<Constraint>());
            sr.getConstraints().add(c);
            sr.setResourceRequests(new ArrayList<ResourceRequest>());
            for(ServiceImplementation sp : s.getImplementationList()){
                Random random = new Random(System.currentTimeMillis());
                ResourceRequest rr = new ResourceRequest();
                rr.setHardwareType(sp.getHardwareType());
                rr.setDisk_IO(random.nextInt(500));
                rr.setMemory(random.nextInt(32));
                rr.setMIPS(random.nextDouble() * 10000);
                rr.setNetBandwidth(random.nextInt(1024));
                sr.getResourceRequests().add(rr);
            }
            template.getServiceRequestList().add(sr);
        }
        return template;
    }

    private Blueprint getBlueprint() {
        Blueprint bp = new Blueprint();
        bp.setId("901238-14091258");
        bp.setName("BP Name");
        bp.setVersion("1.0");

        Service service1 = new Service();
        service1.setName("WebService");

        ServiceImplementation implementation1 = new ServiceImplementation();
        implementation1.setHardwareType(HardwareType.CPU);
        implementation1.setDependencyList(createDependencies());

        service1.setImplementationList(new ArrayList<ServiceImplementation>());
        service1.getImplementationList().add(implementation1);

        /*  */
        Service service2 = new Service();
        service2.setName("RayTracing");

        ServiceImplementation implementation21 = new ServiceImplementation();
        implementation21.setHardwareType(HardwareType.CPU);
        implementation21.setDependencyList(createRayDependencies());

        ServiceImplementation implementation22 = new ServiceImplementation();
        implementation22.setHardwareType(HardwareType.GPU);
        implementation22.setDependencyList(createRayDependencies());

        service2.setImplementationList(new ArrayList<ServiceImplementation>());
        service2.getImplementationList().add(implementation21);
        service2.getImplementationList().add(implementation22);
        /*  */


        bp.setServiceList(new ArrayList<Service>());
        bp.getServiceList().add(service1);
        bp.getServiceList().add(service2);
        return bp;
    }

    private List<Dependency> createRayDependencies() {
        RepoDependency d1 = new RepoDependency();
        d1.setName("cmake.x86_64");
        d1.setVersion("last");
        d1.setRepository("yum");
        RepoDependency d2 = new RepoDependency();
        d2.setName("tbb.x86_64");
        d2.setVersion("last");
        d2.setRepository("yum");
        List<Dependency> deps = new ArrayList<Dependency>(2);
        deps.add(d1);
        deps.add(d2);
        return deps;
    }

    private List<Dependency> createDependencies() {
        RepoDependency d1 = new RepoDependency();
        d1.setName("tomcat");
        d1.setVersion("8");
        d1.setRepository("apt");
        BinaryDependency d2 = new BinaryDependency();
        d2.setName("web-app");
        d2.setUri(URI.create("http://example.com/webapps/name/web-app.war"));
        d2.setInstallScript(URI.create("http://example.com/webapps/name/install.sh"));
        List<Dependency> deps = new ArrayList<Dependency>(2);
        deps.add(d1);
        deps.add(d2);
        return deps;
    }
}
