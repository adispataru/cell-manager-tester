import org.apache.commons.cli.*;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by adispataru on 10-Jun-16.
 */
public class CLI {
    private static final Logger LOG = Logger.getLogger(CLI.class.getName());
    private String[] args = null;
    private Options options = new Options();
    private int numThreads;
    private int timeLax;

    public CLI(String[] args){
        this.args = args;

        options.addOption(numberOfThreads());
        options.addOption(timeLax());
        options.addOption("h", "help", false, "Show help");

    }

    public void parse(){
        CommandLineParser parser = new BasicParser();

        CommandLine cmd = null;
        try {
            cmd = parser.parse(options, args);

            if (cmd.hasOption("h"))
                help();

            if (cmd.hasOption("n")) {
                LOG.log(Level.INFO, "Using cli argument -n=" + cmd.getOptionValue("n"));
                numThreads = Integer.parseInt(cmd.getOptionValue("n"));

            } else {
                LOG.log(Level.SEVERE, "MIssing n option");
                help();
            }

            if(cmd.hasOption("t")){
                LOG.log(Level.INFO, "Using cli argument -t=" + cmd.getOptionValue("t"));
                timeLax = Integer.parseInt(cmd.getOptionValue("t"));
            }



        } catch (ParseException e) {
            LOG.log(Level.SEVERE, "Failed to parse comand line properties", e);
            help();
        }
    }


    private Option numberOfThreads(){
        Option option = OptionBuilder.withArgName("number")
                .hasArg()
                .withDescription("set max number of concurrent users")
                .create("n");
        return option;
    }

    private Option timeLax(){
        Option option = OptionBuilder.withArgName("number")
                .hasArg()
                .withDescription("set max time lax")
                .create("t");
        return option;
    }

    private void help() {
        // This prints out some help
        HelpFormatter formater = new HelpFormatter();

        formater.printHelp("Main", options);
        System.exit(0);
    }


    public int getNumThreads() {
        return numThreads;
    }

    public void setNumThreads(int numThreads) {
        this.numThreads = numThreads;
    }

    public int getTimeLax() {
        return timeLax;
    }

    public void setTimeLax(int timeLax) {
        this.timeLax = timeLax;
    }
}
